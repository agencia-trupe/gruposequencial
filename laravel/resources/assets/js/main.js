import AjaxSetup from "./AjaxSetup";
import MobileToggle from "./MobileToggle";

AjaxSetup();
MobileToggle();

$(document).ready(function () {
  // BANNERS + PAGERS
  $(".cycle-slideshow").cycle({
    slides: ".banner",
    fx: "fade",
  });

  // AVISO DE COOKIES
  $(".aviso-cookies").hide();

  if (window.location.href == routeHome) {
    $(".aviso-cookies").show();

    $(".aceitar-cookies").click(function () {
      var url = window.location.origin + "/aceite-de-cookies";

      $.ajax({
        type: "POST",
        url: url,
        success: function (data, textStatus, jqXHR) {
          $(".aviso-cookies").hide();
        },
        error: function (jqXHR, textStatus, errorThrown) {
          console.log(jqXHR, textStatus, errorThrown);
        },
      });
    });
  }
});
