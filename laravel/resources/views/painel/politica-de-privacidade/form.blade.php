@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('texto_privacidade', 'Texto - Política de Privacidade') !!}
    {!! Form::textarea('texto_privacidade', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_cookies', 'Texto - Política de Cookies') !!}
    {!! Form::textarea('texto_cookies', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}