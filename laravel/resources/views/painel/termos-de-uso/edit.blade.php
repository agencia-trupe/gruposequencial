@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Termos de Uso</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.termos-de-uso.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.termos-de-uso.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
