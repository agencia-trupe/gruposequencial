@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Escola Técnica - Cursos /</small> Editar Curso</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.escola.cursos.update', $escola->id, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.escola.cursos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
