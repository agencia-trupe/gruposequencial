@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Escola Técnica - Cursos /</small> Adicionar Curso</h2>
</legend>

{!! Form::open([
    'route' => ['painel.escola.cursos.store', $escola->id],
    'files' => true
]) !!}

@include('painel.escola.cursos.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection