@extends('painel.common.template')

@section('content')

<legend>
    <h2>Escola Técnica
        <a href="{{ route('painel.escola.cursos.index', $registro->id) }}" class="btn btn-info pull-right">
            <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Cursos
        </a>
    </h2>
</legend>

{!! Form::model($registro, [
'route' => ['painel.escola.update', $registro->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.escola.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection