@extends('painel.common.template')

@section('content')

<legend>
    <h2>Faculdade
        <a href="{{ route('painel.faculdade.cursos.index', $registro->id) }}" class="btn btn-info pull-right">
            <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Cursos
        </a>
    </h2>
</legend>

{!! Form::model($registro, [
'route' => ['painel.faculdade.update', $registro->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.faculdade.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection