@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Faculdade - Cursos /</small> Adicionar Curso</h2>
</legend>

{!! Form::open([
    'route' => ['painel.faculdade.cursos.store', $faculdade->id],
    'files' => true
]) !!}

@include('painel.faculdade.cursos.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection