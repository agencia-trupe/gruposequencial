@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Faculdade - Cursos /</small> Editar Curso</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.faculdade.cursos.update', $faculdade->id, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.faculdade.cursos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
