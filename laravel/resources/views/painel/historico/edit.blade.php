@extends('painel.common.template')

@section('content')

<legend>
    <h2>Nosso Histórico
        <a href="{{ route('painel.historico.imagens.index', $registro->id) }}" class="btn btn-info pull-right">
            <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Imagens
        </a>
    </h2>
</legend>

{!! Form::model($registro, [
'route' => ['painel.historico.update', $registro->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.historico.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection