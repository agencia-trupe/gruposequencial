@extends('painel.common.template')

@section('content')

<legend>
    <h2>EAD
        <a href="{{ route('painel.ead.cursos.index', $registro->id) }}" class="btn btn-info pull-right">
            <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Cursos
        </a>
    </h2>
</legend>

{!! Form::model($registro, [
'route' => ['painel.ead.update', $registro->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.ead.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection