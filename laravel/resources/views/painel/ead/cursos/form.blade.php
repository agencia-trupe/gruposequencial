@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('tipo', 'Tipo de Curso') !!}
    {!! Form::text('tipo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título do Curso') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('link', 'Link do Curso') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.ead.cursos.index', $ead->id) }}" class="btn btn-default btn-voltar">Voltar</a>
