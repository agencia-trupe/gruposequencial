@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>EAD - Cursos /</small> Adicionar Curso</h2>
</legend>

{!! Form::open([
    'route' => ['painel.ead.cursos.store', $ead->id],
    'files' => true
]) !!}

@include('painel.ead.cursos.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection