@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>EAD - Cursos /</small> Editar Curso</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.ead.cursos.update', $ead->id, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.ead.cursos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
