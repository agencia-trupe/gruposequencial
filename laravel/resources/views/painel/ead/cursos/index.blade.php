@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        EAD - Cursos
        <a href="{{ route('painel.ead.cursos.create', $ead->id) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Curso</a>
    </h2>
</legend>


@if(!count($registros))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="ead_cursos">
    <thead>
        <tr>
            <th>Titulo</th>
            <th>Tipo de Curso</th>
            <th>Link do Curso</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($registros as $registro)
        <tr class="tr-row" id="{{ $registro->id }}">
            <td>{{ $registro->titulo }}</td>
            <td>{{ $registro->tipo }}</td>
            <td>{{ $registro->link }}</td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.ead.cursos.destroy', $ead->id, $registro->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.ead.cursos.edit', [$ead->id, $registro->id] ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection