<ul class="nav navbar-nav">
    <li @if(Tools::routeIs('painel.banners*')) class="active" @endif>
        <a href="{{ route('painel.banners.index') }}">Home</a>
    </li>
    <li @if(Tools::routeIs('painel.historico*')) class="active" @endif>
        <a href="{{ route('painel.historico.index') }}">Nosso Histórico</a>
    </li>
    <li @if(Tools::routeIs('painel.faculdade*')) class="active" @endif>
        <a href="{{ route('painel.faculdade.index') }}">Faculdade</a>
    </li>
    <li @if(Tools::routeIs('painel.escola*')) class="active" @endif>
        <a href="{{ route('painel.escola.index') }}">Escola Técnica</a>
    </li>
    <li @if(Tools::routeIs('painel.ead*')) class="active" @endif>
        <a href="{{ route('painel.ead.index') }}">EAD</a>
    </li>
    <li @if(Tools::routeIs('painel.termos-de-uso*')) class="active" @endif>
        <a href="{{ route('painel.termos-de-uso.index') }}">Termos de Uso</a>
    </li>
    <li @if(Tools::routeIs('painel.politica-de-privacidade*')) class="active" @endif>
        <a href="{{ route('painel.politica-de-privacidade.index') }}">Política de Privacidade</a>
    </li>
    <li class="dropdown @if(Tools::routeIs('painel.contato*')) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.contato.index')) class="active" @endif>
                <a href="{{ route('painel.contato.index') }}">Informações de Contato</a>
            </li>
            <li @if(Tools::routeIs('painel.contato.recebidos*')) class="active" @endif>
                <a href="{{ route('painel.contato.recebidos.index') }}">
                    Contatos Recebidos
                    @if($contatosNaoLidos >= 1)
                    <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                    @endif
                </a>
            </li>
        </ul>
    </li>
</ul>