@extends('frontend.common.template')

@section('content')

<div class="ead">
    <div class="img-logo-linha">
        <div class="imagem-logo">
            <img src="{{ asset('assets/img/ead/'.$ead->imagem) }}" class="img-ead" alt="">
            <div class="logo">
                <img src="{{ asset('assets/img/layout/marca-ead-original.svg') }}" alt="" class="img-logo">
            </div>
        </div>
        <hr class="linha-topo">
    </div>

    <div class="frase">
        <p class="p-frase">{{ $ead->frase }}</p>
    </div>

    @if(count($cursos) != 0)
    <div class="cursos">
        <h2 class="titulo">NOSSOS CURSOS À DISTÂNCIA</h2>
        @foreach($cursos as $curso)
        <a href="{{ $curso->link }}" target="_blank" class="curso">{{ $curso->tipo }} em <span class="curso-nome">{{ $curso->titulo }}</span></a>
        @endforeach
    </div>
    @endif

    <div class="saiba-mais">
        <p class="titulo-saiba-mais">SAIBA MAIS NO SITE:</p>
        <a href="http://www.sequencialead.com.br/">
            <img src="{{ asset('assets/img/layout/marca-ead-original.svg') }}" alt="" class="logo">
        </a>
    </div>

    <hr class="linha-fim">

    @include('frontend.caixas-links')

    @include('frontend.central-atendimento')
</div>


@endsection