<div class="caixas-links">
    <a href="https://www.faculdadesequencial.com.br/" target="_blank" class="faculdade">
        <p class="nome">FACULDADE</p>
        <p class="descricao">GRADUAÇÃO E PÓS-GRADUAÇÃO</p>
        <img src="{{ asset('assets/img/layout/marca-faculdade-bco.svg') }}" alt="" class="logo-link">
        <p class="visitar-faculdade">visitar o site <img src="{{ asset('assets/img/layout/setinha-branco.svg') }}" alt=""></p>
    </a>
    <a href="https://www.escolasequencial.com.br/" target="_blank" class="escola">
        <p class="nome">ESCOLA TÉCNICA</p>
        <p class="descricao">CURSOS TÉCNICOS PROFISSIONALIZANTES</p>
        <img src="{{ asset('assets/img/layout/marca-escolatecnica-bco.svg') }}" alt="" class="logo-link">
        <p class="visitar-escola">visitar o site <img src="{{ asset('assets/img/layout/setinha-branco.svg') }}" alt=""></p>
    </a>
    <a href="http://www.sequencialead.com.br/" target="_blank" class="ead">
        <p class="nome">EAD SEQUENCIAL</p>
        <p class="descricao">ENSINO À DISTÂNCIA</p>
        <img src="{{ asset('assets/img/layout/marca-ead-bco.svg') }}" alt="" class="logo-link">
        <p class="visitar-ead">visitar o site <img src="{{ asset('assets/img/layout/setinha-branco.svg') }}" alt=""></p>
    </a>
</div>