@extends('frontend.common.template')

@section('content')

<div class="contato">

    <div class="envio-contato">
        <p class="titulo">CENTRAL DE ATENDIMENTO SEQUENCIAL</p>
        <p class="telefone-contato">11 <span class="destaque">{{ $contato->telefone}}</span></p>
        <form action="{{ route('fale-conosco.post') }}" method="POST">
            <p class="frase">Caso prefira envie-nos uma mensagem:</p>
            {!! csrf_field() !!}
            <div class="dados">
                <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" required>
                <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                <input type="text" name="telefone" value="{{ old('telefone') }}" placeholder="telefone">
            </div>
            <textarea name="mensagem" placeholder="mensagem" required>{{ old('mensagem') }}</textarea>

            <button type="submit" class="btn-contato">ENVIAR</button>
        </form>
        @if($errors->any())
        <div class="flash flash-erro">
            @foreach($errors->all() as $error)
            {!! $error !!}<br>
            @endforeach
        </div>
        @endif

        @if(session('enviado'))
        <div class="flash flash-sucesso">
            <p>Mensagem enviada com sucesso!</p>
            <a href="{{ route('fale-conosco') }}">« voltar</a>
        </div>
        @endif
    </div>

    <hr class="linha-contato">

    @include('frontend.caixas-links')

    @include('frontend.central-atendimento')
</div>

@endsection