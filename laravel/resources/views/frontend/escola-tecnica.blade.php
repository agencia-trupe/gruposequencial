@extends('frontend.common.template')

@section('content')

<div class="escola-tecnica">
    <div class="img-logo-linha">
        <div class="imagem-logo">
            <img src="{{ asset('assets/img/escola/'.$escola->imagem) }}" class="img-escola" alt="">
            <div class="logo">
                <img src="{{ asset('assets/img/layout/marca-escolatecnica-original.svg') }}" alt="" class="img-logo">
            </div>
        </div>
        <hr class="linha-topo">
    </div>

    <div class="frase">
        <p class="p-frase">{{ $escola->frase }}</p>
    </div>

    <div class="cursos">
        <h2 class="titulo">NOSSOS CURSOS TÉCNICOS</h2>
        @foreach($cursos as $curso)
        <a href="{{ $curso->link }}" target="_blank" class="curso">{{ $curso->tipo }} em <span class="curso-nome">{{ $curso->titulo }}</span></a>
        @endforeach
    </div>

    <div class="saiba-mais">
        <p class="titulo-saiba-mais">SAIBA MAIS NO SITE:</p>
        <a href="https://www.escolasequencial.com.br/">
            <img src="{{ asset('assets/img/layout/marca-escolatecnica-original.svg') }}" alt="" class="logo">
        </a>
    </div>

    <hr class="linha-fim">

    @include('frontend.caixas-links')

    @include('frontend.central-atendimento')
</div>


@endsection