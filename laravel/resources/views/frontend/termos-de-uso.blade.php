@extends('frontend.common.template')

@section('content')

<div class="termos-de-uso">

    <h1 class="titulo">TERMOS DE USO</h1>

    <div class="texto">
        {!! $registro->texto !!}
    </div>

</div>

@endsection