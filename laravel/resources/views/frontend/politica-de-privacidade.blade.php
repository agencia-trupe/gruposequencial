@extends('frontend.common.template')

@section('content')

<div class="politica-de-privacidade">

    <h1 class="titulo-privacidade">POLÍTICA DE PRIVACIDADE</h1>

    <div class="texto-privacidade">
        {!! $registro->texto_privacidade !!}
    </div>

    <h1 class="titulo-cookies">POLÍTICA DE COOKIES</h1>

    <div class="texto-cookies">
        {!! $registro->texto_cookies !!}
    </div>

</div>


@endsection