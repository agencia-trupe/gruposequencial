@extends('frontend.common.template')

@section('content')

<div class="home">

    @include('frontend.caixas-links')

    <div class="banners">
        <hr class="linha-banners">
        <div class="centralizado cycle-slideshow" data-slides="> .banner" data-pause-on-hover="true">
            @foreach($banners as $banner)
            @if(!empty($banner->link))
            <a href="{{ $banner->link }}" class="banner link" target="_blank">
                <img src="{{ asset('assets/img/banners/'.$banner->imagem) }}" class="imagem" alt="">
            </a>
            @else
            <div class="banner">
                <img src="{{ asset('assets/img/banners/'.$banner->imagem) }}" class="imagem" alt="">
            </div>
            @endif
            @endforeach
        </div>
    </div>

    @include('frontend.central-atendimento')
</div>

@endsection