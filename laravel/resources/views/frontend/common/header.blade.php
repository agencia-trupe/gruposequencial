<header>
    <nav>
        @include('frontend.common.nav')
    </nav>
    <button id="mobile-toggle" type="button" role="button">
        <span class="lines"></span>
    </button>
    <a href="{{ route('home') }}" class="logo" title="{{ $config->title }}"></a>
</header>