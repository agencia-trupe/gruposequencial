<footer>
    <div class="footer">
        <div class="links">
            <a href="https://www.faculdadesequencial.com.br/" target="_blank" class="link">FACULDADE SEQUENCIAL</a>
            <hr class="hr-links">
            <a href="https://www.escolasequencial.com.br/" target="_blank" class="link">ESCOLA TÉCNICA SEQUENCIAL</a>
            <hr class="hr-links">
            <a href="http://www.sequencialead.com.br/" target="_blank" class="link">EAD SEQUENCIAL</a>
        </div>

        <div class="politica-termos">
            <a href="{{ route('politica-de-privacidade') }}" class="link-politica">NOSSA POLÍTICA DE PRIVACIDADE</a>
            <hr class="hr-divisao">
            <a href="{{ route('termos-de-uso') }}" class="link-termos">TERMOS DE USO</a>
        </div>

        <hr class="linha-footer">
        <div class="direitos">
            <p>© {{ date('Y') }} <span>{{ config('app.name') }}</span> &middot; Todos os direitos reservados | Criação de sites: <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a></p>
        </div>
    </div>
</footer>