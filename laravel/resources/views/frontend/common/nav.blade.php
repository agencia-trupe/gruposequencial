<div class="itens-menu">
    <a href="{{ route('nosso-historico') }}" @if(Tools::routeIs('nosso-historico')) class="active" @endif>NOSSO HISTÓRICO</a>
    <hr class="hr-links">
    <a href="{{ route('faculdade') }}" @if(Tools::routeIs('faculdade')) class="active" @endif>FACULDADE</a>
    <hr class="hr-links">
    <a href="{{ route('escola-tecnica') }}" @if(Tools::routeIs('escola-tecnica')) class="active" @endif>ESCOLA TÉCNICA</a>
    <hr class="hr-links">
    <a href="{{ route('ead') }}" @if(Tools::routeIs('ead')) class="active" @endif>ENSINO À DISTÂNCIA</a>
    <hr class="hr-links">
    <a href="{{ route('fale-conosco') }}" @if(Tools::routeIs('fale-conosco')) class="active" @endif>FALE CONOSCO</a>
</div>