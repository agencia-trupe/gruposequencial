@extends('frontend.common.template')

@section('content')

<div class="nosso-historico">
    <div class="frase-linha">
        <div class="frase">
            <p class="frase">{{ $historico->frase }}</p>
        </div>
        <hr class="linha-nosso-historico">
    </div>

    <div class="imagens-texto">
        <div class="imagens">
            @foreach($imagens as $imagem)
            <img src="{{ asset('assets/img/historico/thumbs/'.$imagem->imagem) }}" class="imagem" alt="">
            @endforeach
        </div>
        <div class="texto">
            {!! $historico->texto !!}
        </div>
    </div>

    @include('frontend.caixas-links')

    @include('frontend.central-atendimento')

</div>

@endsection