<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    protected $namespace = 'App\Http\Controllers';

    public function boot(Router $router)
    {
        $router->model('banners', 'App\Models\Banner');
        $router->model('historico', 'App\Models\Historico');
        $router->model('historico_imagens', 'App\Models\HistoricoImagem');
        $router->model('faculdade', 'App\Models\Faculdade');
        $router->model('faculdade_cursos', 'App\Models\FaculdadeCurso');
        $router->model('escola', 'App\Models\Escola');
        $router->model('escola_cursos', 'App\Models\EscolaCurso');
        $router->model('ead', 'App\Models\Ead');
        $router->model('ead_cursos', 'App\Models\EadCurso');
        $router->model('termos_de_uso', 'App\Models\TermosDeUso');
        $router->model('politica_de_privacidade', 'App\Models\PoliticaDePrivacidade');
        $router->model('configuracoes', 'App\Models\Configuracao');
        $router->model('contato', 'App\Models\Contato');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('usuarios', 'App\Models\User');

        parent::boot($router);
    }

    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
