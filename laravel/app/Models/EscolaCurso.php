<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EscolaCurso extends Model
{
    protected $table = 'escola_cursos';

    protected $guarded = ['id'];

    public function scopeEscola($query, $id)
    {
        return $query->where('escola_id', $id);
    }
}
