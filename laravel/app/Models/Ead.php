<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class Ead extends Model
{
    protected $table = 'ead';

    protected $guarded = ['id'];

    public function cursos()
    {
        return $this->hasMany('App\Models\EadCurso', 'ead_id');
    }

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
            'width'  => 600,
            'height' => null,
            'path'    => 'assets/img/ead/'
        ]);
    }
}
