<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class HistoricoImagem extends Model
{
    protected $table = 'historico_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeHistorico($query, $id)
    {
        return $query->where('historico_id', $id);
    }

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
            [
                'width'   => 230,
                'height'  => 160,
                'path'    => 'assets/img/historico/thumbs/'
            ],
            [
                'width'  => 1980,
                'height' => null,
                'upsize'  => true,
                'path'    => 'assets/img/historico/'
            ]
        ]);
    }
}
