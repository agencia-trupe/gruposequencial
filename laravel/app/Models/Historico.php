<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Historico extends Model
{
    protected $table = 'historico';

    protected $guarded = ['id'];

    public function imagens()
    {
        return $this->hasMany('App\Models\HistoricoImagem', 'historico_id')->ordenados();
    }
}
