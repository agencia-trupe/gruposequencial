<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EadCurso extends Model
{
    protected $table = 'ead_cursos';

    protected $guarded = ['id'];

    public function scopeEad($query, $id)
    {
        return $query->where('ead_id', $id);
    }
}
