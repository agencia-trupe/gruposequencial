<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class Escola extends Model
{
    protected $table = 'escola';

    protected $guarded = ['id'];

    public function cursos()
    {
        return $this->hasMany('App\Models\EscolaCurso', 'escola_id');
    }

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
            'width'  => 600,
            'height' => null,
            'path'    => 'assets/img/escola/'
        ]);
    }
}
