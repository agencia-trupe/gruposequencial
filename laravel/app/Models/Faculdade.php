<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class Faculdade extends Model
{
    protected $table = 'faculdade';

    protected $guarded = ['id'];

    public function cursos()
    {
        return $this->hasMany('App\Models\FaculdadeCurso', 'faculdade_id');
    }

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
                'width'  => 600,
                'height' => null,
                'path'    => 'assets/img/faculdade/'
        ]);
    }
}
