<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TermosDeUso extends Model
{
    protected $table = 'termos_de_uso';

    protected $guarded = ['id'];
}
