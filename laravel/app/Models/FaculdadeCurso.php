<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FaculdadeCurso extends Model
{
    protected $table = 'faculdade_cursos';

    protected $guarded = ['id'];

    public function scopeFaculdade($query, $id)
    {
        return $query->where('faculdade_id', $id);
    }
}
