<?php

namespace App\Http\Controllers;

use App\Models\Historico;
use App\Models\HistoricoImagem;

class NossoHistoricoController extends Controller
{
    public function index()
    {
        $historico = Historico::first();

        $imagens = HistoricoImagem::ordenados()->get();

        return view('frontend.nosso-historico', compact('historico', 'imagens'));
    }
}
