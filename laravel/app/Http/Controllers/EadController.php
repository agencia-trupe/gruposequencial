<?php

namespace App\Http\Controllers;

use App\Models\Ead;
use App\Models\EadCurso;

class EadController extends Controller
{
    public function index()
    {
        $ead = Ead::first();

        $cursos = EadCurso::orderBy('titulo', 'ASC')->get();
        // dd(count($cursos));

        return view('frontend.ead', compact('ead', 'cursos'));
    }
}
