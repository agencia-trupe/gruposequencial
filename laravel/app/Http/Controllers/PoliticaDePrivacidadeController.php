<?php

namespace App\Http\Controllers;

use App\Models\PoliticaDePrivacidade;

class PoliticaDePrivacidadeController extends Controller
{
    public function index()
    {
        $registro = PoliticaDePrivacidade::first();

        return view('frontend.politica-de-privacidade', compact('registro'));
    }
}
