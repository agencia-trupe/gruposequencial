<?php

namespace App\Http\Controllers;

use App\Models\Faculdade;
use App\Models\FaculdadeCurso;

class FaculdadeController extends Controller
{
    public function index()
    {
        $faculdade = Faculdade::first();

        $cursos = FaculdadeCurso::orderBy('titulo', 'ASC')->get();

        return view('frontend.faculdade', compact('faculdade', 'cursos'));
    }
}
