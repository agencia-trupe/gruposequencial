<?php

namespace App\Http\Controllers;

use App\Models\AceiteDeCookies;
use App\Models\Banner;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $banners = Banner::ordenados()->get();
        $verificacao = AceiteDeCookies::where('ip', $request->ip())->first();

        return view('frontend.home', compact('banners', 'verificacao'));
    }

    public function postCookies(Request $request)
    {
        $input = $request->all();
        $input['ip'] = $request->ip();

        AceiteDeCookies::create($input);

        return redirect('/');
    }
}
