<?php

namespace App\Http\Controllers;

use App\Models\Escola;
use App\Models\EscolaCurso;

class EscolaController extends Controller
{
    public function index()
    {
        $escola = Escola::first();

        $cursos = EscolaCurso::orderBy('titulo', 'ASC')->get();

        return view('frontend.escola-tecnica', compact('escola', 'cursos'));
    }
}
