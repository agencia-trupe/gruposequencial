<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\EadCursosRequest;
use App\Models\Ead;
use App\Models\EadCurso;

class EadCursosController extends Controller
{
    public function index(Ead $ead)
    {
        $registros = EadCurso::orderBy('titulo', 'ASC')->get();

        return view('painel.ead.cursos.index', compact('registros', 'ead'));
    }

    public function create(Ead $ead)
    {
        return view('painel.ead.cursos.create', compact('ead'));
    }

    public function store(Ead $ead, EadCursosRequest $request)
    {
        try {
            $input = $request->all();
            $input['ead_id'] = $ead->id;

            EadCurso::create($input);

            return redirect()->route('painel.ead.cursos.index', $ead->id)->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Ead $ead, $registro)
    {
        $registro = EadCurso::where('id', $registro)->first();

        return view('painel.ead.cursos.edit', compact('registro', 'ead'));
    }

    public function update(Ead $ead, EadCursosRequest $request, $registro)
    {
        try {
            $registro = EadCurso::where('id', $registro)->first();

            $input = $request->all();
            $input['ead_id'] = $ead->id;

            $registro->update($input);

            return redirect()->route('painel.ead.cursos.index', $ead->id)->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Ead $ead, $registro)
    {
        try {
            $registro = EadCurso::where('id', $registro)->first();

            $registro->delete();

            return redirect()->route('painel.ead.cursos.index', $ead->id)->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
