<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\TermosDeUsoRequest;
use App\Models\TermosDeUso;

class TermosDeUsoController extends Controller
{
    public function index()
    {
        $registro = TermosDeUso::first();

        return view('painel.termos-de-uso.edit', compact('registro'));
    }

    public function update(TermosDeUsoRequest $request, TermosDeUso $registro)
    {
        try {
            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.termos-de-uso.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
