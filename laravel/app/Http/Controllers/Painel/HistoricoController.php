<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\HistoricoRequest;
use App\Models\Historico;

class HistoricoController extends Controller
{
    public function index()
    {
        $registro = Historico::first();

        return view('painel.historico.edit', compact('registro'));
    }

    public function update(HistoricoRequest $request, Historico $registro)
    {
        try {
            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.historico.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
