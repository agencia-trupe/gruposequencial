<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\HistoricoImagensRequest;
use App\Models\Historico;
use App\Models\HistoricoImagem;

class HistoricoImagensController extends Controller
{
    public function index(Historico $registro)
    {
        $imagens = HistoricoImagem::historico($registro->id)->ordenados()->get();

        return view('painel.historico.imagens.index', compact('imagens', 'registro'));
    }

    public function show(Historico $registro, HistoricoImagem $imagem)
    {
        return $imagem;
    }

    public function store(Historico $registro, HistoricoImagensRequest $request)
    {
        try {
            $input = $request->all();
            $input['imagem'] = HistoricoImagem::uploadImagem();
            $input['historico_id'] = $registro->id;

            $imagem = HistoricoImagem::create($input);

            $view = view('painel.historico.imagens.imagem', compact('registro', 'imagem'))->render();

            return response()->json(['body' => $view]);
        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: ' . $e->getMessage();
        }
    }

    public function destroy(Historico $registro, HistoricoImagem $imagem)
    {
        try {
            $imagem->delete();

            return redirect()->route('painel.historico.imagens.index', $registro)
                ->with('success', 'Imagem excluída com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: ' . $e->getMessage()]);
        }
    }

    public function clear(Historico $registro)
    {
        try {
            $registro->imagens()->delete();

            return redirect()->route('painel.historico.imagens.index', $registro)
                ->with('success', 'Imagens excluídas com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: ' . $e->getMessage()]);
        }
    }
}
