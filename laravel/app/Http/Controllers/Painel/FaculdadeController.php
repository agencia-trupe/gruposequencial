<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\FaculdadeRequest;
use App\Models\Faculdade;

class FaculdadeController extends Controller
{
    public function index()
    {
        $registro = Faculdade::first();

        return view('painel.faculdade.edit', compact('registro'));
    }

    public function update(FaculdadeRequest $request, Faculdade $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Faculdade::uploadImagem();

            $registro->update($input);

            return redirect()->route('painel.faculdade.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
