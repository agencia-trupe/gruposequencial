<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\EadRequest;
use App\Models\Ead;

class EadController extends Controller
{
    public function index()
    {
        $registro = Ead::first();

        return view('painel.ead.edit', compact('registro'));
    }

    public function update(EadRequest $request, Ead $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Ead::uploadImagem();

            $registro->update($input);

            return redirect()->route('painel.ead.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
