<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\EscolaCursosRequest;
use App\Models\Escola;
use App\Models\EscolaCurso;

class EscolaCursosController extends Controller
{
    public function index(Escola $escola)
    {
        $registros = EscolaCurso::orderBy('titulo', 'ASC')->get();

        return view('painel.escola.cursos.index', compact('registros', 'escola'));
    }

    public function create(Escola $escola)
    {
        return view('painel.escola.cursos.create', compact('escola'));
    }

    public function store(Escola $escola, EscolaCursosRequest $request)
    {
        try {
            $input = $request->all();
            $input['escola_id'] = $escola->id;

            EscolaCurso::create($input);

            return redirect()->route('painel.escola.cursos.index', $escola->id)->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Escola $escola, $registro)
    {
        $registro = EscolaCurso::where('id', $registro)->first();

        return view('painel.escola.cursos.edit', compact('registro', 'escola'));
    }

    public function update(Escola $escola, EscolaCursosRequest $request, $registro)
    {
        try {
            $registro = EscolaCurso::where('id', $registro)->first();

            $input = $request->all();
            $input['escola_id'] = $escola->id;

            $registro->update($input);

            return redirect()->route('painel.escola.cursos.index', $escola->id)->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Escola $escola, $registro)
    {
        try {
            $registro = EscolaCurso::where('id', $registro)->first();

            $registro->delete();

            return redirect()->route('painel.escola.cursos.index', $escola->id)->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
