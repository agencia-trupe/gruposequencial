<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\FaculdadeCursosRequest;
use App\Models\Faculdade;
use App\Models\FaculdadeCurso;

class FaculdadeCursosController extends Controller
{
    public function index(Faculdade $faculdade)
    {
        $registros = FaculdadeCurso::orderBy('titulo', 'ASC')->get();

        return view('painel.faculdade.cursos.index', compact('registros', 'faculdade'));
    }

    public function create(Faculdade $faculdade)
    {
        return view('painel.faculdade.cursos.create', compact('faculdade'));
    }

    public function store(Faculdade $faculdade, FaculdadeCursosRequest $request)
    {
        try {
            $input = $request->all();
            $input['faculdade_id'] = $faculdade->id;

            FaculdadeCurso::create($input);

            return redirect()->route('painel.faculdade.cursos.index', $faculdade->id)->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Faculdade $faculdade, $registro)
    {
        $registro = FaculdadeCurso::where('id', $registro)->first();
        
        return view('painel.faculdade.cursos.edit', compact('registro', 'faculdade'));
    }

    public function update(Faculdade $faculdade, FaculdadeCursosRequest $request, $registro)
    {
        try {
            $registro = FaculdadeCurso::where('id', $registro)->first();

            $input = $request->all();
            $input['faculdade_id'] = $faculdade->id;

            $registro->update($input);

            return redirect()->route('painel.faculdade.cursos.index', $faculdade->id)->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Faculdade $faculdade, $registro)
    {
        try {
            $registro = FaculdadeCurso::where('id', $registro)->first();
            
            $registro->delete();

            return redirect()->route('painel.faculdade.cursos.index', $faculdade->id)->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
