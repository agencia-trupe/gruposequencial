<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\EscolaRequest;
use App\Models\Escola;

class EscolaController extends Controller
{
    public function index()
    {
        $registro = Escola::first();

        return view('painel.escola.edit', compact('registro'));
    }

    public function update(EscolaRequest $request, Escola $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Escola::uploadImagem();

            $registro->update($input);

            return redirect()->route('painel.escola.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
