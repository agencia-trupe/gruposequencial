<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('nosso-historico', 'NossoHistoricoController@index')->name('nosso-historico');
    Route::get('faculdade', 'FaculdadeController@index')->name('faculdade');
    Route::get('escola-tecnica', 'EscolaController@index')->name('escola-tecnica');
    Route::get('ead', 'EadController@index')->name('ead');
    Route::get('fale-conosco', 'ContatoController@index')->name('fale-conosco');
    Route::post('fale-conosco', 'ContatoController@post')->name('fale-conosco.post');
    Route::get('politica-de-privacidade', 'PoliticaDePrivacidadeController@index')->name('politica-de-privacidade');
    Route::get('termos-de-uso', 'TermosDeUsoController@index')->name('termos-de-uso');
    
    Route::post('aceite-de-cookies', 'HomeController@postCookies')->name('aceite-de-cookies.post');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function () {
        Route::get('/', 'PainelController@index')->name('painel');
        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
        Route::resource('banners', 'BannersController');
        Route::resource('historico', 'HistoricoController', ['only' => ['index', 'update']]);
        Route::get('historico/{historico}/imagens/clear', [
            'as'   => 'painel.historico.imagens.clear',
            'uses' => 'HistoricoImagensController@clear'
        ]);
        Route::resource('historico.imagens', 'HistoricoImagensController', ['parameters' => ['imagens' => 'historico_imagens']]);
        Route::resource('faculdade', 'FaculdadeController', ['only' => ['index', 'update']]);
        Route::resource('faculdade.cursos', 'FaculdadeCursosController');
        Route::resource('escola', 'EscolaController', ['only' => ['index', 'update']]);
        Route::resource('escola.cursos', 'EscolaCursosController');
        Route::resource('ead', 'EadController', ['only' => ['index', 'update']]);
        Route::resource('ead.cursos', 'EadCursosController');
        Route::resource('termos-de-uso', 'TermosDeUsoController', ['only' => ['index', 'update']]);
        Route::resource('politica-de-privacidade', 'PoliticaDePrivacidadeController', ['only' => ['index', 'update']]);
        Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);
        Route::resource('usuarios', 'UsuariosController');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function () {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
