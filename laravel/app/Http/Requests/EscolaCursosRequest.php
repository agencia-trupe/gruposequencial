<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EscolaCursosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'tipo'   => 'required',
            'titulo' => 'required',
            'link'   => 'required',
        ];
    }
}
