<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EadRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'imagem' => 'required|image',
            'frase'  => 'required'
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }
}
