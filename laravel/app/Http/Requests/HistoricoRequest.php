<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class HistoricoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'frase'  => 'required',
            'texto'  => 'required',
        ];

        return $rules;
    }
}
