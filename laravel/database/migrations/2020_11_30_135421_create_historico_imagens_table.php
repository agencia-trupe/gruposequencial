<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateHistoricoImagensTable extends Migration
{
    public function up()
    {
        Schema::create('historico_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('historico_id')->unsigned();
            $table->foreign('historico_id')->references('id')->on('historico')->onDelete('cascade');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('historico_imagens');
    }
}
