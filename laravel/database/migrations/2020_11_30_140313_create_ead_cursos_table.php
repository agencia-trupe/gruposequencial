<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateEadCursosTable extends Migration
{
    public function up()
    {
        Schema::create('ead_cursos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ead_id')->unsigned();
            $table->foreign('ead_id')->references('id')->on('ead')->onDelete('cascade');
            $table->string('tipo');
            $table->string('titulo');
            $table->string('link');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('ead_cursos');
    }
}
