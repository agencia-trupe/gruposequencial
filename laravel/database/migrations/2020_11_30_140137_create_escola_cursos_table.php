<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateEscolaCursosTable extends Migration
{
    public function up()
    {
        Schema::create('escola_cursos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('escola_id')->unsigned();
            $table->foreign('escola_id')->references('id')->on('escola')->onDelete('cascade');
            $table->string('tipo');
            $table->string('titulo');
            $table->string('link');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('escola_cursos');
    }
}
