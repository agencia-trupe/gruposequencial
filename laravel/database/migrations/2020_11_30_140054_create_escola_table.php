<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateEscolaTable extends Migration
{
    public function up()
    {
        Schema::create('escola', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imagem');
            $table->string('frase');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('escola');
    }
}
