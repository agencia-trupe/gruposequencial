<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreatePoliticaDePrivacidadeTable extends Migration
{
    public function up()
    {
        Schema::create('politica_de_privacidade', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto_privacidade');
            $table->text('texto_cookies');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('politica_de_privacidade');
    }
}
