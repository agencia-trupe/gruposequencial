<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateFaculdadeCursosTable extends Migration
{
    public function up()
    {
        Schema::create('faculdade_cursos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('faculdade_id')->unsigned();
            $table->foreign('faculdade_id')->references('id')->on('faculdade')->onDelete('cascade');
            $table->string('tipo');
            $table->string('titulo');
            $table->string('link');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('faculdade_cursos');
    }
}
