<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PoliticaDePrivacidadeSeeder extends Seeder
{
    public function run()
    {
        DB::table('politica_de_privacidade')->insert([
            'texto_privacidade' => '',
            'texto_cookies'     => '',
        ]);
    }
}
