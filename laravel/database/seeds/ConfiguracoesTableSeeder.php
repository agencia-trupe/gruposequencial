<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConfiguracoesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('configuracoes')->insert([
            'title' => 'Grupo Sequencial',
            'description' => '',
            'keywords' => '',
            'imagem_de_compartilhamento' => '',
            'analytics' => '',
        ]);
    }
}
