<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContatoTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('contato')->insert([
            'nome' => 'Grupo Sequencial',
            'email' => 'contato@trupe.net',
            'telefone' => '3371·2828',
            'horario' => 'Segunda à Sexta · das 7h às 21h',
        ]);
    }
}
