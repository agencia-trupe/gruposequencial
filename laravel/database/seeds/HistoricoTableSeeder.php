<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HistoricoTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('historico')->insert([
            'frase' => 'Uma história de ousadia, superação e liderança alicerçada em quatro valores fundamentais: o saber, a ética, o trabalho e o progresso.',
            'texto' => '',
        ]);
    }
}
