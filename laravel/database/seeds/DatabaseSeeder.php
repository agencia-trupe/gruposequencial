<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ConfiguracoesTableSeeder::class);
        $this->call(ContatoTableSeeder::class);
        $this->call(HistoricoTableSeeder::class);
        $this->call(TermosDeUsoTableSeeder::class);
        $this->call(PoliticaDePrivacidadeSeeder::class);
        $this->call(FaculdadeTableSeeder::class);
        $this->call(EscolaTableSeeder::class);
        $this->call(EadTableSeeder::class);
    }
}
